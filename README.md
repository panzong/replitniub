# 参数

- 地址：**xxx.repl.co**
- 端口：**443**
- 用户ID：**cb2ef9cb-fc3a-4f42-a206-0a59919a38d6**（默认）
- 传输协议：**ws**
- 传输层安全：**tls**
- 路径：
  - vless：**/**
  - vmess：**/vmess**
  - trojan：**/trojan**

> 其他参数默认
